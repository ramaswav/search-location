# LIVE APP

https://search-location-262504.web.app/

# Beam Trace

This is a SPA built on react firebase and deployed on GCP. It fetches the nearest scooter locations for a given lat/lang.
In order for the app to be fail safe, if you do not enter a valid lat/lang it will display all the locations it has which is 24 now.

## Tech Stack

#### Front-end

* The front-end client is built as a simple-page-application using React
* Material design bootstrap is used for page styling.

#### Back-end

* The back-end server is built with Express.js and Node.js.
* Its deployed as a cloud function on Google Cloud Platform.
* Routes are not auth protected as of now.

#### Database

* MySQL is used a database to store the location because it provides us with scientific functions which are used to determine nearest scooter locations.

## Usage

### Running Locally
1. Install Node.js v10.18.0 ( Note this code is not working on V13.0 beause of a package incompatibility, please use V10).
2. `git clone https://ramaswav@bitbucket.org/ramaswav/search-location.git`;
3. cd into the directory and run `yarn install`;
4. cd into the functions directory `npm install`;
5. run local node server: nodemon local-server-not-deployable.js
6. cd into root directory and then run `yarn run start`;
7. Application should be running at http://localhost:3000/ if you dont have anything else there.

### Test Application Functionality  API's

1. Select number of scooters you want to see
2. select distance from your coordinates in KM
3. Enter this coordinates:
`Hougang 1 Mall singapore`
`LATITUDE-- 1.3758`
`LONGITUDE-- 103.8794`

### List of locations:
|    location_name       | latitude|  longitude |
|:---------------------- |:------- |:-----------|
| Changi Airport         |1.359167|103.989441   |
| Gardens by the Bay     |1.282375|103.864273   |
| Sentosa                |1.250111|103.830933   |
| Ang Mo Kio             |1.369115|103.845436   |
| Changi                 |1.345010|103.983208   |
| Hougang                |1.371778|103.893059   |
| Downtown Core          |1.287953|103.851784   |
| Management University  |1.296568|103.852119   |
| Yu Hua Secondary School|1.347026|103.724052   |
| Marina Bay Sands Hotel |1.282302|103.858528   |
| Seletar Airport (XSP)  |1.420181|103.864555   |
| Senoko Road            |1.463780|103.801811   |
| Tiger Sky Tower        |1.254975|103.817596   |
| Hotel Miramar          |1.288710|103.837372   |
| National Library       |1.297588|103.854309   |
| Rajdee Peninsula Plaza |1.292307|103.850441   |
| Swiss�tel The Stamford |1.293354|103.853561   |
| Changi Airport         |1.359167|103.989441   |
| AMK Hub                |1.369309|103.848351   |
| Orchard Road           |1.304833|103.831833   |
| Admiralty Park         |1.446392|103.780655   |
| Mount Faber Park       |1.273806|103.817497   |
| Gleneagles Medical Cent|1.307222|103.819725   |
| Yio Chu Kang           |1.366700|103.800003   |
| Cagnes-sur-Mer France  |43.664398|7.148900    |
| Marina Bay Sands Hotel |1.282302|103.858528   |
| Swiss�tel The Stamford |1.293354|103.853561   |
| Hotel Miramar          |1.288710|103.837372   |
| Orchard Roa            |1.304833|103.831833   |
| Senoko Road            |1.463780|103.801811   |


## Backend API's
|   Route       | METHOD |  Description                                               |
|:------------- |:------ |:---------------------------------------------------------- |
| /places       | POST   |Takes in input filters and returns nearest scooters.        |