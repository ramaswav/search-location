const functions = require('firebase-functions')
const express = require('express')
const bodyParser = require('body-parser')
const mysql = require('mysql')
const _ = require('lodash')
const request = require('request')

/**
 * TODO(developer): specify SQL connection details
 */
const connectionName = process.env.INSTANCE_CONNECTION_NAME
const dbUser = process.env.SQL_USER
const dbPassword = process.env.SQL_PASSWORD
const dbName = process.env.SQL_NAME
const mysqlConfig = {
  connectionLimit: 1,
  user: dbUser,
  password: dbPassword,
  database: dbName,
}

if (process.env.NODE_ENV === 'production') {
  mysqlConfig.socketPath = `/cloudsql/${connectionName}`
}

// Connection pools reuse connections between invocations,
// and handle dropped or expired connections automatically.
let connection

if (!connection) {
  connection = mysql.createPool(mysqlConfig)
}

const app = express()
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Credentials', true)
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

app.post('/popular', function(req, res) {
  if (
    req.body.latitude !== '' &&
    req.body.longitude !== '' &&
    !isNaN(parseFloat(req.body.latitude)) &&
    !isNaN(parseFloat(req.body.longitude))
  ) {
    query1 =
      `SELECT * FROM (SELECT *,Round(( ( ( acos( Sin(( ` +
      req.body.latitude +
      ` * Pi() / 180)) * Sin(( latitude * Pi() / 180)) + Cos(( ` +
      req.body.latitude +
      ` * Pi() /180 )) * Cos(( latitude * Pi() / 180)) * Cos((( ` +
      req.body.longitude +
      ` - longitude) * Pi()/180))) ) * 180/Pi() ) * 60 * 1.1515 * 1.609344 ),1) AS distance_in_km FROM popular_places ) as popular_places_distance WHERE  Round(distance_in_km,1) <= ` +
      req.body.distance_range +
      ` LIMIT ` +
      req.body.number_of_scooters
  } else {
    query1 = `SELECT * FROM popular_places`
  }

  connection.query(query1, function(error, results, fields) {
    let resultobj = {
      places: results,
    }
    if (error) throw error
    res.send(resultobj)
  })
})

exports.app = functions.https.onRequest(app)
