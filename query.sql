
SELECT * FROM (
    SELECT *, 
        (
            (
                (
                    acos(
                        sin(( 1.3758 * pi() / 180))
                        *
                        sin(( `latitude` * pi() / 180)) + cos(( 1.3758 * pi() /180 ))
                        *
                        cos(( `latitude` * pi() / 180)) * cos((( 103.8794 - `longitude`) * pi()/180)))
                ) * 180/pi()
            ) * 60 * 1.1515 * 1.609344
        )
    as distance_in_km FROM `popular_places`
) popular_places
WHERE round(distance_in_km,1) <= 5