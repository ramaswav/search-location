import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Navbar from './components/common/Navbar'
import Footer from './components/common/Footer'
import Places from './components/places/Places'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <div>
            <div>
              <Navbar />
            </div>
            <main>
              <Switch>
                <Route exact path="/" component={Places} />
              </Switch>
            </main>
            <Footer />
          </div>
        </div>
      </BrowserRouter>
    )
  }
}

export default App
