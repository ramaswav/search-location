import React from 'react'
import { MDBFooter } from 'mdbreact'

const Footer = props => {
  const date = new Date().getFullYear()
  return (
    <MDBFooter style={{ ...props.style, zIndex: 2 }}>
      <p className="footer-copyright mb-0 py-3 text-center">
        &copy; {date} Copyright:{' '}
        <a href="https://www.linkedin.com/in/vivramaswamy/">
          {' '}
          Vivek Ramaswamy{' '}
        </a>
      </p>
    </MDBFooter>
  )
}
export default Footer
