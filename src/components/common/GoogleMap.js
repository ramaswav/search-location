import React, { Component } from 'react'
import { compose } from 'recompose'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps'
import { MDBPopoverBody, MDBPopoverHeader } from 'mdbreact'

function createMarkup(html) {
  return { __html: html }
}

const MapWithAMarker = compose(
  withScriptjs,
  withGoogleMap
)(props => {
  return (
    <GoogleMap defaultZoom={11.5} defaultCenter={props.defaultCenter}>
      {props.markers.map(marker => {
        const onClick = props.onClick.bind(this, marker)
        return (
          <Marker
            key={Math.random()}
            onClick={onClick}
            position={{ lat: marker.latitude, lng: marker.longitude }}
          >
            {props.selectedMarker === marker && (
              <InfoWindow>
                <div style={{ color: '#276b82', fontSize: 25 }}>
                  <MDBPopoverHeader>{marker.popover_header}</MDBPopoverHeader>
                  <MDBPopoverBody>
                    <div
                      dangerouslySetInnerHTML={createMarkup(
                        marker.popover_body
                      )}
                    />
                  </MDBPopoverBody>
                </div>
              </InfoWindow>
            )}
          </Marker>
        )
      })}
    </GoogleMap>
  )
})

export default class GMap extends Component {
  constructor(props) {
    super(props)
    this.state = {
      locations: this.props.locations,
      selectedMarker: true,
    }
  }
  componentDidMount() {}
  componentWillReceiveProps(nextprops) {
    let mutated_state = this.state
    mutated_state.locations = nextprops.locations
    this.setState(mutated_state)
  }

  handleClick = (marker, event) => {
    this.setState({ selectedMarker: marker })
  }

  render() {
    return (
      <MapWithAMarker
        selectedMarker={this.state.selectedMarker}
        markers={this.state.locations}
        onClick={this.handleClick}
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDd5_psT9oLNRdX45T7j4yPqfTLhePbDl8"
        defaultCenter={this.props.defaultCenter}
        loadingElement={<div style={{ height: `100%`, width: `100%` }} />}
        containerElement={<div style={{ height: `800px`, width: `100%` }} />}
        mapElement={<div style={{ height: `100%`, width: `100%` }} />}
        frameborder="0"
        style={{ border: 0 }}
        allowfullscreen
      />
    )
  }
}
