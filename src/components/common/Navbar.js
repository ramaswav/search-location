import React, { Component } from 'react'
import { MDBContainer, MDBNavbar, MDBRow, MDBCol } from 'mdbreact'
import { Link } from 'react-router-dom'
import logo from '../../assets/logo.png'

class Navbar extends Component {
  render() {
    return (
      <div>
        <MDBNavbar dark expand="sm" fixed="top">
          <MDBContainer>
            <MDBRow className="mt-2" style={{ width: '100%' }}>
              <MDBCol md="1" sm="12" className="mt-2 text-center">
                <img alt="logo" height={20} src={logo} />
              </MDBCol>
              <MDBCol md="10" sm="12" className="mt-1 text-center">
                <Link to="/">
                  <h3 className="nav-title h3-responsive my-auto">
                    Beam Trace
                  </h3>
                </Link>
              </MDBCol>
              <MDBCol md="1" sm="12" className="mt-0 text-center"></MDBCol>
            </MDBRow>
          </MDBContainer>
        </MDBNavbar>
      </div>
    )
  }
}
export default Navbar
