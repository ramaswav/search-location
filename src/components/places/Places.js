import React, { Component } from 'react'
import {
  MDBRow,
  MDBContainer,
  MDBIcon,
  MDBCol,
  MDBBtn,
  MDBSelect,
  MDBInput,
} from 'mdbreact'
import GMap from '../common/GoogleMap'
import Utils from '../common/Utils'

class Places extends Component {
  constructor(props) {
    super(props)
    //Define State.
    this.state = {
      //Number of scooters state
      number_of_scooters: {
        value: '',
        temp: [
          {
            text: '1',
            value: '1',
          },
          {
            text: '2',
            value: '2',
          },
          {
            text: '3',
            value: '3',
          },
          {
            text: '4',
            value: '4',
          },
          {
            text: '5',
            value: '5',
          },
          {
            text: '6',
            value: '6',
          },
          {
            text: '7',
            value: '7',
          },
          {
            text: '8',
            value: '8',
          },
          {
            text: '9',
            value: '9',
          },
          {
            text: '10',
            value: '10',
          },
        ],
      },
      //Distance Range in KM.
      distance_range: {
        value: '',
        temp: [
          {
            text: '< 1 Kilometer',
            value: '1',
          },
          {
            text: '< 2 Kilometer',
            value: '2',
          },
          {
            text: '< 3 Kilometer',
            value: '3',
          },
          {
            text: '< 4 Kilometer',
            value: '4',
          },
          {
            text: '< 5 Kilometer',
            value: '5',
          },
          {
            text: '< 6 Kilometer',
            value: '6',
          },
          {
            text: '< 7 Kilometer',
            value: '7',
          },
          {
            text: '< 8 Kilometer',
            value: '8',
          },
          {
            text: '< 9 Kilometer',
            value: '9',
          },
          {
            text: '< 10 Kilometer',
            value: '10',
          },
        ],
      },
      latitude: '',
      longitude: '',
      placesLocation: [],
      isLoadingPlaces: true,
    }
  }

  //Input handler for number of scooters selection
  setValueOfSelectedSoocterNumber(value) {
    let mutated_state = this.state
    mutated_state.number_of_scooters.value = value[0]
    this.setState(mutated_state)
  }

  //Input handler for distance selection
  setValueOfSelectedDistanceRange(value) {
    let mutated_state = this.state
    mutated_state.distance_range.value = value[0]
    this.setState(mutated_state)
  }

  setValueOfSelectedLatitude(event) {
    let mutated_state = this.state
    mutated_state.latitude = event.target.value
    this.setState(mutated_state)
  }

  setValueOfSelectedLongitude(event) {
    let mutated_state = this.state
    mutated_state.longitude = event.target.value
    this.setState(mutated_state)
  }

  plotLocations() {
    let mutated_state = this.state
    let postData = {
      number_of_scooters: this.state.number_of_scooters.value,
      distance_range: this.state.distance_range.value,
      latitude: this.state.latitude,
      longitude: this.state.longitude,
    }
    mutated_state.isLoadingPlaces = true
    this.setState(mutated_state, function() {
      fetch(Utils.apiUrl + 'popular', {
        headers: {
          Accept: 'application/json, */*',
          'Content-Type': 'application/json',
        },
        method: 'post',
        body: JSON.stringify(postData),
      })
        .then(function(response) {
          return response.json()
        })
        .then(function(locationdata) {
          let locationsToPlot = []
          locationdata.places.forEach(function(location, index) {
            let location_obj = {
              id: index,
              popover_body:
                ' <p> <b>Location Name:</b>' + location.location_name + ' </p>',
              popover_header: location.location_name,
              longitude: parseFloat(location.longitude),
              latitude: parseFloat(location.latitude),
            }
            locationsToPlot.push(location_obj)
          })
          return locationsToPlot
        })
        .then(function(locationsToPlot) {
          mutated_state.placesLocation = locationsToPlot
          mutated_state.isLoadingPlaces = false
          this.setState(mutated_state)
        }.bind(this))
    })
  }

  componentDidMount() {}

  render() {
    return (
      <MDBRow>
        <MDBContainer>
          <MDBCol md="12" className="border-1 z-depth-1 p-4" md="12">
            <MDBRow>
              <MDBCol md="3">
                <MDBSelect
                  search
                  color="secondary"
                  getValue={this.setValueOfSelectedSoocterNumber.bind(this)}
                  options={this.state.number_of_scooters.temp}
                  label="Number of Scooters"
                />
              </MDBCol>
              <MDBCol md="3">
                <MDBSelect
                  search
                  color="secondary"
                  getValue={this.setValueOfSelectedDistanceRange.bind(this)}
                  options={this.state.distance_range.temp}
                  label="Within Kilometer"
                />
              </MDBCol>
              <MDBCol md="3">
                <MDBInput
                  label="Latitude"
                  onChange={this.setValueOfSelectedLatitude.bind(this)}
                  icon="location"
                />
              </MDBCol>
              <MDBCol md="3">
                <MDBInput
                  label="Longitude"
                  onChange={this.setValueOfSelectedLongitude.bind(this)}
                  icon="location"
                />
              </MDBCol>
              <MDBCol md="12" className="text-center-button">
                <MDBBtn onClick={this.plotLocations.bind(this)} size="md">
                  {' '}
                  <MDBIcon far icon="clone" />
                  &nbsp;Explore
                </MDBBtn>
              </MDBCol>
            </MDBRow>
          </MDBCol>

          <MDBCol
            md="12"
            style={{ padding: '0%' }}
            className="mt-5 border-1 z-depth-1 p-0"
            md="12"
          >
            {this.state.isLoadingPlaces ? (
              Utils.renderSpinner()
            ) : (
              <div
                id="map-container-google-3"
                className="z-depth-1-half map-container"
              >
                {' '}
                <GMap
                  locations={this.state.placesLocation}
                  defaultCenter={{ lat: 1.29027, lng: 103.851959 }}
                />
              </div>
            )}
          </MDBCol>
        </MDBContainer>
      </MDBRow>
    )
  }
}

export default Places
